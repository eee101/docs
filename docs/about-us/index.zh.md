---
template: about-us.html

about:
  title: 关于我们
  content:
    - "终身学习俱乐部（LLC）是南洋理工大学电气与电子工程学院的官方硕士研究生社团。我们的使命是创造一个支持性的环境，让硕士研究生在学术、职业和个人发展方面都能茁壮成长。我们提供各种周到的服务和活动，帮助他们顺利融入校园生活。"
    - "此外，我们通过强调技能培养和行业参与来解决硕士研究生体验中的关键问题。通过丰富的项目、工作坊和活动，我们为学生提供发展宝贵技能、了解行业动态以及与同行和专业人士建立持久联系的机会。"
    - "我们致力于促进持续学习和个人成长，期待在硕士研究生群体中培养对 LLC 和南洋理工大学的归属感。"

units:
  - id: ec
    name: EC
    fullName: 执行委员会
    intro: "负责俱乐部的整体战略规划、政策制定和重要决策，确保可持续发展和长期愿景的实现。主要职责包括："
    responsibilities:
      - 制定俱乐部发展战略和年度计划
      - 监督各团队的工作执行情况
      - 管理俱乐部预算和资源分配
      - 建立和维护重要合作关系
  - id: sec
    name: SEC
    fullName: 秘书处
    intro: "处理日常行政事务、会员管理、财务管理和对外联络。主要职责包括："
    responsibilities:
      - 管理会员注册和信息维护
      - 处理日常财务交易和记录
      - 协调内部沟通和文档管理
      - 维护与学校和外部机构的关系
  - id: emt
    name: EMT
    fullName: 活动管理团队
    intro: "策划和执行各类学习活动、工作坊、讲座和社交活动。主要职责包括："
    responsibilities:
      - 设计和组织各类学习活动
      - 管理活动预算和资源
      - 收集反馈并持续改进
      - 建立活动评估体系
    members:
      - name: Jackie Yang
        position: 团队负责人
        email: yingjie002@e.ntu.edu.sg
        avatar: /assets/images/members/dit/download.jpeg
  - id: dit
    name: DIT
    fullName: 数字与信息技术团队
    intro: "负责俱乐部的数字建设、网站维护和技术支持。主要职责包括："
    responsibilities:
      - 开发和维护俱乐部网站
      - 提供技术支持和故障排除
      - 管理俱乐部社交媒体账号
      - 开发和维护数字工具
    members:
      - name: Jackie Yang
        position: 团队负责人
        email: yingjie002@e.ntu.edu.sg
        avatar: /assets/images/members/dit/download.jpeg
      - name: Yee Jiann
        position: 团队成员
        email: EMAILu.edu.sg
        avatar: /assets/images/members/dit/download.jpeg
      - name: Yee Jiann
        position: 团队成员
        email: EMAILu.edu.sg
        avatar: /assets/images/members/dit/download.jpeg
      - name: Yee Jiann
        position: 团队成员
        email: EMAILu.edu.sg
        avatar: /assets/images/members/dit/download.jpeg
      - name: Yee Jiann
        position: 团队成员
        email: EMAILu.edu.sg
        avatar: /assets/images/members/dit/download.jpeg

connect:
  title: 联系我们
  intro: "在 LLC，我们专门为电气与电子工程学院的硕士生组织各种社交和学术活动，同时提供一系列服务，旨在让你的南大生活更顺畅、更愉快。与 LLC 保持联系，发现更多活动，分享你的建议和反馈，真正提升你的研究生生活！"
  contact_title: "更多信息，请联系："
  contacts:
    - type: 邮箱
      value: eee-lll_student_club@ntu.edu.sg
      link: mailto:eee-lll_student_club@ntu.edu.sg
    - type: 实验室
      value: LLL@EEE (S2.1-B4-02)
  qrcodes:
    - name: 微信
      image: /assets/images/qr-codes/wechat.png
    - name: 领英
      image: /assets/images/qr-codes/linkedin.png
      link: "https://www.linkedin.com/company/ntu-llc"
    - name: 小红书
      image: /assets/images/qr-codes/rednote.png
      link: "https://www.xiaohongshu.com/user/profile/5f31de11000000000100750b"
    - name: Instagram
      image: /assets/images/qr-codes/instagram.png
      link: "https://www.instagram.com/lll_eee_ntu/"
---

# 关于我们  <!-- do not remove -->