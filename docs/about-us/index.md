---
template: about-us.html

about:
  title: About Us
  content:
    - "The Lifelong Learning Club (LLC) is an official student club for MSc graduate students of the School of Electrical and Electronic Engineering at NTU. Our mission is to create a supportive environment where MSc graduate students can thrive academically, professionally, and personally. We offer a variety of thoughtful services and activities designed to help them smoothly integrate into campus life."
    - "In addition, we address key issues in the MSc graduate student experience by placing a strong emphasis on skill-building and industry engagement. Through a wide range of programs, workshops, and events, we provide opportunities for students to develop valuable skills, gain insights into the industry, and build lasting connections with peers and professionals."
    - "Committed to fostering continuous learning and personal growth, we look forward to cultivating a sense of belonging to LLC and NTU among the MSc graduate student population."

units:
  - id: ec
    name: EC
    fullName: Executive Committee
    intro: "Responsible for the overall strategic planning, policy making and key decisions of the club, ensuring sustainable development and realization of long-term vision. Main responsibilities include:"
    responsibilities:
      - Develop club development strategies and annual plans
      - Monitor the work execution of each team
      - Manage club budget and resource allocation
      - Establish and maintain key partnerships
  - id: sec
    name: SEC
    fullName: Secretariat
    intro: "Handle daily administrative affairs, membership management, financial management, and liaison with external organizations. Main responsibilities include:"
    responsibilities:
      - Manage member registration and information maintenance
      - Handle daily financial transactions and records
      - Coordinate internal communication and document management
      - Maintain relationships with school and external institutions
  - id: emt
    name: EMT
    fullName: Event Management Team
    intro: "Plan and execute various learning activities, workshops, lectures and social events. Main responsibilities include:"
    responsibilities:
      - Design and organize various learning activities
      - Manage event budgets and resources
      - Collect feedback and continuously improve
      - Establish event evaluation system
    members:
      - name: Jackie Yang
        position: Team Leader
        email: yingjie002@e.ntu.edu.sg
        avatar: ../assets/images/members/dit/download.jpeg
  - id: dit
    name: DIT
    fullName: Digital & Information Technology Team
    intro: "Responsible for the club's digital construction, website maintenance, and technical support. Main responsibilities include:"
    responsibilities:
      - Develop and maintain club's website
      - Provide technical support and troubleshooting
      - Manage club's social media accounts
      - Develop and maintain digital tools
    members:
      - name: Jackie Yang
        position: Team Leader
        email: yingjie002@e.ntu.edu.sg
        avatar: ../assets/images/members/dit/download.jpeg
      - name: Yee Jiann
        position: Team Member
        email: EMAILu.edu.sg
        avatar: ../assets/images/members/dit/download.jpeg
      - name: Yee Jiann
        position: Team Member
        email: EMAILu.edu.sg
        avatar: ../assets/images/members/dit/download.jpeg
      - name: Yee Jiann
        position: Team Member
        email: EMAILu.edu.sg
        avatar: ../assets/images/members/dit/download.jpeg
      - name: Yee Jiann
        position: Team Member
        email: EMAILu.edu.sg
        avatar: ../assets/images/members/dit/download.jpeg

connect:
  title: Connect to Us
  intro: "At LLC, we organize a variety of social and academic events specifically for EEEE MSc students, while also offering a range of services designed to make your NTU experience smoother and more enjoyable. Stay connected with LLC to discover more events, share your suggestions and feedback, and truly enhance your graduate life!"
  contact_title: "For more information, please contact:"
  contacts:
    - type: Email
      value: eee-lll_student_club@ntu.edu.sg
      link: mailto:eee-lll_student_club@ntu.edu.sg
    - type: Lab space
      value: LLL@EEE (S2.1-B4-02)
  qrcodes:
    - name: WeChat
      image: /assets/images/qr-codes/wechat.png
    - name: LinkedIn
      image: /assets/images/qr-codes/linkedin.png
      link: "https://www.linkedin.com/company/ntu-llc"
    - name: RedNote
      image: /assets/images/qr-codes/rednote.png
      link: "https://www.xiaohongshu.com/user/profile/5f31de11000000000100750b"
    - name: Instagram
      image: /assets/images/qr-codes/instagram.png
      link: "https://www.instagram.com/lll_eee_ntu/"
---

# About Us  <!-- do not remove -->