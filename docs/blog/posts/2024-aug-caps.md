---
date: 2024-03-30
authors: [qq]
description: >
  Announcing the CAPS for 2024 August intake MSc students.
categories:
  - MSc
  - Student Service
---

# Announcing 2024 CAPS for August-Intake MSc

We are proudly announcing this year's CAPS(1) for August-intake MSc students.
{ .annotate }

1. Complimentary Airport Pickup Service, exclusive for EEE MSc students.

Students are enjoying a smooth transportation experience from overseas flights to NTU campus by using both CAPS and CHECKS(1) -- upon successful registration. Note that only those who successfully utilized CAPS can enjoy CHECKS.
{ .annotate }

1. Campus Housing Early-Collection of Key Service, exclusive for EEE MSc students.

<!-- more -->

Promotion eDM is as shown below. Scanning the QR code brings up CAPS registration form.

![2024 CAPS for August-Intake MSc](caps-edm/2024-caps-aug-poster-pre-registration.png)