---
date: 2025-02-11
authors: [qq]
description: >
  2025 Chinese New Year Celebration Carnival Event Report
categories:
  - MSc
  - Events
  - CNY
---

# 2025 EEE MSc Chinese New Year Celebration Carnival

![](2025-cny-celebration/group-photo.png){ .rounded }

We are delighted to announce the successful completion of the 2025 Chinese New Year Celebration Carnival. This first-ever outdoor casual carnival attracted 285 participants, with over 90% from EEE MSc programs.

<!-- more -->

## __Event Details__

- **Date:** February 10, 2025
- **Time:** 14:00 - 18:00
- **Venue:** NTU TCT-LT
- **Participants:** 285 attendees
- **Organizer:** Lifelong Learning Club @ EEE
- **Activities:** 
    - Holiday-themed game stalls
    - Student performances
    - Lucky draw
    - Food & beverages

## __Photo Gallery__
!!! gallery ""
    ![](./2025-cny-celebration/photo-1.jpg){ width="49%" style="object-fit:cover;aspect-ratio:7/5;" }
    ![](./2025-cny-celebration/photo-2.jpg){ width="49%" style="object-fit:cover;aspect-ratio:7/5;" }
    ![](./2025-cny-celebration/photo-3.jpg){ width="49%" style="object-fit:cover;aspect-ratio:7/5;" }
    ![](./2025-cny-celebration/photo-4.jpg){ width="49%" style="object-fit:cover;aspect-ratio:7/5;" }
    ![](./2025-cny-celebration/photo-5.jpg){ width="49%" style="object-fit:cover;aspect-ratio:7/5;" }
    ![](./2025-cny-celebration/photo-6.jpg){ width="49%" style="object-fit:cover;aspect-ratio:7/5;" }
    ![](./2025-cny-celebration/photo-7.jpg){ width="49%" style="object-fit:cover;aspect-ratio:7/5;margin-right:0.9%" }
    ![](./2025-cny-celebration/photo-8.jpg){ width="49%" style="object-fit:cover;aspect-ratio:7/5;" }

## __Event Videos__

<video width="640" height="360" controls >
  <source src="/blog/2025-cny-celebration/2025-cny-celebration.mp4" type="video/mp4">
  您的浏览器不支持 HTML5 视频。
</video>