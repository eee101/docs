---
date: 2024-04-27
authors: [qq]
description: >
  Announcing the 2024 MSc Graduation Dinner at EEE
categories:
  - MSc
  - Graduation
---

# 2024 EEE MSc Graduation Dinner

![](2024-msc-graduation-dinner/pan-pacific-singapore.avif){ .rounded }

We are proudly announcing the 2024 Graduation Dinner for all EEE MSc students at Pan Pacific Singapore on 11th May 2024 Saturday.

<!-- more -->

Eligible students are:

__Graduate__

:   MSc students who are graduating from EEE this semester.

__Non-Graduate__

:   MSc students who are not graduating from EEE this semester.

    Non-graduates include EEE students who are working on exams or dissertations, without explicitly acknowledgements of either final transcripts or dissertation archived by NTU Library.

__Partner__

:   Partners are non-EEE personnel brought in to the dinner event by an EEE graduate or non-graduate. Partners must be registered and paid via a valid EEE MSc student.

## __Date & Time__

The dinner is to be hosted on 11 May 2024 18:00 to 22:00.

## __Venue & Transportation__

The dinner is hosted at Pan Pacific Singapore, a descent leisure place with fine cuisine.

<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d13224.322564248818!2d103.8481101847577!3d1.2936521066416666!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31da19a8f0c90bb1%3A0xd68bb4dc42f9f2f4!2sPan%20Pacific%20Singapore!5e0!3m2!1sen!2ssg!4v1714203470294!5m2!1sen!2ssg" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>

We do not provide transportation. You and your partner are advised to take public transportation to reach the venue.

## __Dinner__

We offer eight-course exquisite Chinese table dinner at the event, with optional Halal and vegetarian menus. Professional emcee and Jazz band are standing by to delighten the atmosphere.

Soft drink, beer, and wine are provided during the event.

!!! danger "Safety Notice"

    Drunk driving is a serious offense in Singapore and is liable to SGD 1,000 to SGD 30,000 fine and/or up to 12 months imprisonment.

## __Gifts__

We provide exlusive non-commercial door gifts, including:

- Lyon, the NTU official mascots, EEE limited edition
- Photo Frame. Acrony photo frame with NTU logo, kindly sponsored by the NTU Alumni Engagement Office.

We also provide a number of &ldquo;good&rdquo; gifts during the lucky draw session:

- Apple<sup>&reg;</sup> iPad 10th Generation
- Garmin<sup>&reg;</sup> Lily Watch
- Jo Malone<sup>&reg;</sup> Cologne
- Huawei<sup>&reg;</sup> FreeBuds SE 2 Earphone

## __Registration__

All EEE MSc students can register a seat at [here](https://wis.ntu.edu.sg/webexe88/owa/REGISTER_NTU.REGISTER?EVENT_ID=OA24040314274978). However, we accept a maximum capacity of 200 students/partners. If you are at a position beyond 200, you may be put in the waitlist.

A non-EEE MSc personnel is not eligible to register our graduation dinner. He/she must be registered as a partner of an EEE student.

Registration is only finalized when all fees are paid accordingly.

## __Fees__

We charge SGD 50.00 (9% GST inclusive) on each participant. EEE students are required to pay their fees, as well as their partners', in cash to the __Administrative Office at S1-B1A-32__. An e-receipt will be issued to the payer at the end of our financial process in mid or late May.

!!! info "Refunding Policy"

    We are happy to refund full entry fee to all _graduates_, as defined at the beginning of this article. Non-graduates or partners are not eligible for refunding.