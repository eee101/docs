# Online Verification

New students are required to verify the authenticity of their documents submitted at the application stage via online verification.

Please note that 2024 fall students will be allowed to __Begin Online Verification__ after __June 15, 2024__. It can be modified in the subsequent __Online Matriculation__ phase if information is incorrectly filled out.

The purpose of this article is to guide you through the online verification process step by step.

!!! note

    - __BE PATIENT__, The online verification page may take a long time to load.
    - The information __CAN NOT be modified__ after submission except by uploading documents.
    - Official online verification details [click here](online-verification/online-verification-details.pdf).

## Sign in E-Verification Portal

Use your Application Number, Passport and Date of Birth to sign in via [E-Verification portal](https://venus.wis.ntu.edu.sg/GEVER/GEVER_StudentStartPage.aspx). The passport number and date of birth will be displayed after clicking the Go button after filling in the application number.

<figure markdown="span">
    ![](online-verification/sign-in.png){ width=360 .bordered .rounded }
</figure>

## Check Information

Your personal information will be displayed after sign in, please check carefully that the corresponding information is correct, otherwise contact addmission_coursework@ntu.edu.sg for feedback.

Please note that the __Date of Registration is normal be empty__ if you come into this page for the first time. It will be displayed after you submit the online verification form.

<figure markdown="span">
    ![](online-verification/information-check.png){ .bordered .rounded }
</figure>

## Finish E-Verification Form

The Personal Particulars, Employment in NTU, Eligibility for MOE Subsidy, For Full-Time Student Only and Declaration sections have been skipped. Please fill in these sections yourself as they are not prone to problems. Further guidance on other sections is provided below.

### Contact Info

Click the __Prompt Symbol__ to the left of __Home Country Address__, you can see that international students should only fill in their home address. However, this may make it impossible to save and submit due to system issues. Therefore, please fill in __both your Home Address and Singapore Address__ in the format as shown below.

<figure markdown="span">
    ![](online-verification/contact-information.png){ .bordered .rounded }
</figure>

### Emergency Contacts

Please provide __ONLY ONE__ address for your emergency contacts, either Singapore or Foreign. Do not Provide both.

<figure markdown="span">
    ![](online-verification/emergency-contacts.png){ .bordered .rounded }
</figure>

### Upload Documents

This section is used to upload your relevant materials to the school for verification. Please make sure that the uploaded documents is __Clearly Visible__, otherwise it may lead to audit failure.

Please submit the __Official Documents in English__. Otherwise, provide __Original Documents and Translations__ of its.

!!! warning "Ensure your documents meet these requirements"

    - File size must not be more than __2MB__.
    - File name must be in __English Characters__.
    - Upload of documents will still be available after you have completed online verification.

Check out the __Conditional Offer__ part if you are fresh graduate, otherwise, check out the __Direct Offer__ part. The materials to be uploaded are listed below.

<figure markdown="span">
    ![](online-verification/online-verification-details-documents-need.png){ width=680 }
</figure>

For students using international qualifications,  the __Degree Verification Report__ from NTU approved verification agency varies for students from different countries.

  - Chinese Universities to go through: [China Higher-education Student Information (CHSI)](https://my.chsi.com.cn/archive/gjhz/foreign/choose.action)

    > Note: Request verification report which names __Online Verification Report of Higher Education Degree Certificate__, not Graduate Certificate or Transcripts. Please ensure that the verification report is __in English__ and __valid__ to verification.

  - Australian/ New Zealand Universities to go through: [My eQuals](https://www.myequals.edu.au/)

    > Note: Please upload the academic documents/links with the PIN in a word document to the e-verification portal.

  - United States of America Universities to go through: [National Student Clearinghouse](https://www.studentclearinghouse.org/)

    > Note: Please request degree verify certificate instead of enrolment certificate. If you are unable to perform the verification through this agency, kindly proceed with Risk Management Intelligence (RMI) instead.

  - For other Universities to go through: [Risk Management Intelligence (RMI)](https://rmi.com.sg/ntu/) or [World Education Services Inc. (WES)](https://www.wes.org/partners/credential-evaluation-requirements-for-nanyang-technological-university-ntu/)

    > Note: Do note that there will be fees involve using the above degree verification agency.


!!! Question "Confused about what needs to be submitted?"

    As a simple example, if you are a fresh graduate from China, then you need to submit:

      - Passport (bio page with photo and personal details)
      - Student Pass
      - Official Degree Certificate
      - Official Transcript
      - Degree Verification Report from CHSI

    Please note that the content and naming of these documents should be __in English__.

## Result

After submitting your verification, you will not receive any confirmation message. You can sign in again to check if the submission was successful.
