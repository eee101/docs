# 在线验证

新生需要通过在线验证来确认申请阶段提交的文件的真实性。

请注意，2024年秋季入学的学生将在 __2024年6月15日__ 之后才能 __开始在线验证__。如果信息填写错误，可以在后续的 __在线学籍注册__ 阶段进行修改。

本文将为您逐步介绍在线验证流程。

!!! note "注意"

    - __请耐心等待__，在线验证页面可能需要较长时间加载。
    - 提交后信息 __无法修改__，除非通过上传文件。
    - 官方在线验证详情[点击这里](online-verification/online-verification-details.pdf)。

## 登录在线验证系统

使用您的申请编号、护照和出生日期通过[在线验证系统](https://venus.wis.ntu.edu.sg/GEVER/GEVER_StudentStartPage.aspx)登录。填写申请编号并点击继续按钮后，将显示护照号码和出生日期。

<figure markdown="span">
    ![](online-verification/sign-in.png){ width=360 .bordered .rounded }
</figure>

## 检查信息

登录后将显示您的个人信息，请仔细检查相应信息是否正确，如有问题请联系 addmission_coursework@ntu.edu.sg 反馈。

请注意，如果您是第一次访问此页面，__注册日期通常为空__。提交在线验证表格后将显示该日期。

<figure markdown="span">
    ![](online-verification/information-check.png){ .bordered .rounded }
</figure>

## 完成在线验证表格

个人资料、南大就业情况、教育部补贴资格、仅限全日制学生和声明部分已被略过。请自行填写这些部分，因为它们不容易出现问题。以下提供其他部分的进一步指导。

### 联系信息

点击 __原籍国地址__ 左侧的 __提示符号__，您可以看到国际学生应该只填写其原籍国地址。但是，由于系统问题，这可能导致无法保存和提交。因此，请按照下图所示格式 __同时填写原籍国地址和新加坡地址__。

<figure markdown="span">
    ![](online-verification/contact-information.png){ .bordered .rounded }
</figure>

### 紧急联系人

请 __只提供一个__ 紧急联系人地址，可以是新加坡或国外地址。请勿同时提供两者。

<figure markdown="span">
    ![](online-verification/emergency-contacts.png){ .bordered .rounded }
</figure>

### 上传文件

此部分用于上传相关材料供学校验证。请确保上传的文件 __清晰可见__，否则可能导致审核失败。

请提交 __英文版官方文件__。如果没有，请提供 __原始文件及其翻译件__。

!!! warning "确保您的文件满足以下要求"

    - 文件大小不得超过 __2MB__。
    - 文件名必须使用 __英文字符__。
    - 完成在线验证后仍可上传文件。

如果您是应届毕业生，请查看 __有条件录取__ 部分，否则请查看 __直接录取__ 部分。需要上传的材料如下所示。

<figure markdown="span">
    ![](online-verification/online-verification-details-documents-need.png){ width=680 }
</figure>

对于使用国际学历的学生，来自南大认可的验证机构的 __学位验证报告__ 因不同国家而异。

  - 中国高校需通过：[中国高等教育学生信息网（学信网）](https://my.chsi.com.cn/archive/gjhz/foreign/choose.action)

    > 注意：请申请名为 __高等教育学位证书在线验证报告__ 的验证报告，而不是毕业证书或成绩单。请确保验证报告 __使用英文__ 且 __在有效期内__。

  - 澳大利亚/新西兰高校需通过：[My eQuals](https://www.myequals.edu.au/)

    > 注意：请将带有PIN码的学术文件/链接以word文档形式上传到在线验证系统。

  - 美国高校需通过：[National Student Clearinghouse](https://www.studentclearinghouse.org/)

    > 注意：请申请学位验证证书而不是在读证明。如果无法通过该机构进行验证，请改用Risk Management Intelligence (RMI)。

  - 其他高校需通过：[Risk Management Intelligence (RMI)](https://rmi.com.sg/ntu/) 或 [World Education Services Inc. (WES)](https://www.wes.org/partners/credential-evaluation-requirements-for-nanyang-technological-university-ntu/)

    > 注意：使用上述学位验证机构将产生费用。

!!! Question "不确定需要提交什么？"

    举个简单的例子，如果您是来自中国的应届毕业生，那么您需要提交：

      - 护照（带照片和个人信息的个人资料页）
      - 学生准证
      - 官方学位证书
      - 官方成绩单
      - 学信网学位验证报告

    请注意，这些文件的内容和命名都应该 __使用英文__。

## 结果

提交验证后，您不会收到任何确认消息。您可以重新登录以检查提交是否成功。