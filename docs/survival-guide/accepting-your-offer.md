# Accepting Your Offer

Congratulations! You have offered admission into NTU as MSC full-time student in the School of Electrical & Electronic Engineering. Now you can get ready to follow this guide to accept your offer and start an exciting journey.

!!! note

    You could start your operation as follows only when you receive an email named _Nanyang Technological University (NTU) Offer of Admission To Coursework Programme (C0000000)_.

## Download Offer Letter

The first thing you need to do is __download your official offer letter__. This document contains important information about your admission and the steps you need to follow next.

A copy of your offer of admission can be downloaded in the [admission portal](https://wis.ntu.edu.sg/webexe/owa/pga$cw_eoffer_display.main). It is normal if you do not see your offer in the portal immediately. It may take __1~3 work days__ for the system to update.

Click the __red link__ (D:\EOFFER_C0000000_000.pdf) to download your offer after you sign in the admission portal.

<figure markdown="span">
    ![](accepting-your-offer/download-offer-letter.png){ .bordered .rounded }
</figure>

## Accept Your Offer

After you receive your acceptance email named __Nanyang Technological University (NTU) Offer of Admission To Coursework Programme (C0000000)__, you could accept your offer. Please make sure you do this __before the deadline__ on this email, otherwise it means you are automatically declining the offer.

__Accept your offer__ in the [accept now portal](https://wis.ntu.edu.sg/webexe/owa/pgr$acceptance_offer.main).

<figure markdown="span">
    ![](accepting-your-offer/accept-offer.png){ .bordered .rounded }
</figure>

After accepting the offer, you will receive an __email confirmation__ and you can also __check Your offer status__ via [acceptance status portal](https://wis.ntu.edu.sg/webexe/owa/pgr$adm_acceptform.main).

<figure markdown="span">
    ![](accepting-your-offer/check-offer-status.png){ .bordered .rounded }
</figure>

## Pay Acceptance Deposit

In order to book your place in the program, you are required to pay a non-refundable acceptance fee of __$5,000__ (inclusive of GST) to confirm your acceptance of the offer. The acceptance fee will be used to offset the tuition fees upon matriculation in first semester of the program.

!!! warning

    Please pay the acceptance deposit after __receiving deposit email__ and __DO NOT__ miss the deadline.

You may pay the acceptance deposit via the [online payment portal](https://ntuadminonestop.service-now.com/ntusp?id=ntu_adhoc_payment ). Select the corresponding program name and click pay button to proceed. For instance, choose __Deposit-EEE MSC in Computer Control & Automation programmes__ if you are admitted to MSC in __Computer Control & Automation__.

<figure markdown="span">
    ![](accepting-your-offer/deposit-payment-entrance.png){ .bordered .rounded }
</figure>

Fill out the online payment form and choose PayNow or Visa / Master card to pay. Please note that you should fill in your __Application Number__ at the __Reference ID__ and do not make a mistake.

<figure markdown="span">
    ![](accepting-your-offer/deposit-payment-form.png){ .bordered .rounded }
</figure>

Retain a __copy of your receipt__ that you received in case of any verifications required.

<figure markdown="span">
    ![](accepting-your-offer/receipt.png){ width=500 .bordered .rounded }
</figure>


## Read Welcome Letter

You will also receive a welcome letter after the school has decided to offer you admission. This letter will contain important information about your program, including the start date, orientation, and other essential details.

<figure markdown="span">
    ![](accepting-your-offer/welcome-letter.png){ width=800 .bordered .rounded }
</figure>
