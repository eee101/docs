# Health Screening

New students must pass the health screening to fulfill all requirements of the Student's Pass formality set by the Immigration & Checkpoint Authority (ICA).

NTU students are required to perform this health screening at the University Health Service Center (Fullerton@NTU), unless he/she possesses a valid medical certificate from recognized Singapore clinics.

This article guides you through the health screening step by step.

!!! info "Mandatory Health Screening"

    The health screening is one of the mandatory steps in the Student's Pass application process. For NTU students, you MUST complete the health screening in Singapore, either at the University Health Service (Fullerton@NTU) or at another certified local clinic. No overseas medical report is recognized for health screening.

    Please call the Fullerton@NTU directly at 6793 6828 or email them at: ntu@fullertonhealth.com if you require assistance for the booking of your health screening appointment.

## Account Registration

You should register your account at [Fullerton@NTU](https://bookappt.fullertonhealth.com/#/login).

<figure markdown="span">
    ![](health-screening/sign-up.png){ width=300 .bordered .rounded }
</figure>

Fill in your personal particulars in the Sign Up form. For international students, select __Passport Number__ in the _Identification Type_ section and fill in your passport number.

In the _Company_ section, type in "__NTU__" and wait for one or two seconds until the system automatically suggests the correct option __NTU - NANYANG TECHNOLOGICAL UNIVERSITY__. Select it.

<figure markdown="span">
    ![](health-screening/sign-up-detail.png){ width=400 .bordered .rounded }
</figure>

## Login

In the login UI, choose __Corporate Customer__ and type in the phone number you registered. DO NOT forget to include the __Country Prefix__. The initial password is your date of birth, in the form of __DDMMYYYY__. For example, if you were born on _1st December 2000_, then your initial password is `01122000`.

<figure markdown="span">
    ![](health-screening/login.png){ width=300 .bordered .rounded }
</figure>

## Book an Appointment

In the __Book an Appointment__ page, you must choose __Admission Health Screening 2024A__ in the __Appointment Type__ section. Please __Book an Appointment for Morning__ if you are an international student so that you can have your __X-ray__ done.

<figure markdown="span">
    ![](health-screening/book-appointment.png){ width=400 .bordered .rounded }
</figure>

!!! question "Cannot find _2024A_ or _2024A_ is fully booked"

    If you forgot to choose the _Corporate Customer_ type in the login UI, there will be no such designated _Admission Health Screening 2024A_ option shown for you. Please log off and re-login using the correct options.

    If _2024A_ is fully booked, try to book _2024B_ and _2024C_. These two options are new to address the overbooking of _2024A_.

Continue to complete the booking form. Note that you should type in your __Application Number__ (`Cxxxxxxx`) in the __Additional Comments__ text box.

When finishing this form, click the __Book Appointment__ button to submit. A confirmation email will be sent to your registered email address.

<figure markdown="span">
    ![](health-screening/book-appointment-2.png){ width=400 .bordered .rounded }
</figure>

## Edit Appointment

If you prefer to choose another time slot for the health screening, you can edit your existing appointment on the __My Appointment__ page. Note that you must complete the edit operation at least 48 hours in advance of your preferred new time. Otherwise, you will have to book a new appointment.

<figure markdown="span">
    ![](health-screening/edit-appointment.png){ width=400 .bordered .rounded }
</figure>

## Process Health Screening

On the day of your appointment, you are advised to arrive at Fullerton@NTU about half an hour ahead of your booked time slot - do not arrive too early, or you will be asked not to join the queue.

You should take a printed version of the Medical Checklists, i.e., the [__MC1__ (version 2022)](health-screening/MC1.pdf) and [__MC2__ (version 2022)](health-screening/MC2.pdf).

In MC2 - entitled _Medical Examination Report_ - you should only fill in the first section _Personal Particulars_ and leave all the rest blank.

<figure markdown="span">
    ![](health-screening/mc2.png){ width=400 .bordered .rounded }
</figure>

!!! warning "Document Version"

    You should check if the MCs are in accordance with the latest document version.

## Fees

There is a charge for students who are completing Health screening at Fullerton@NTU.

| Student Type                                                                                | Fee |
|---------------------------------------------------------------------------------------------|:---:|
| Singapore Citizen <br/> Singapore Permanent Resident <br/> Part Time International Students | $32 |
| Full Time International Students                                                            | $50 |

You are advised to bring either cash or a valid VISA / MasterCard / American Express credit card with you to pay for these fees.

A digital invoice will be sent to your registered email.

## Results

Outcomes of the health screening are usually released in __3 Work Days__ after you have completed all examination items. The following _physical_ medical examination reports will be ready for you to collect at the Fullerton@NTU reception:

* Medical examination report (MC2)
* Chest scan result
* HIV test result

Once you have retrieved your results, scan the results/medical reports and upload them to your Student's Pass application on the ICA website so that you can continue with the application process.
