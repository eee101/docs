# 在线学籍注册

学籍注册是一个正式的程序，学生通过这个程序在大学登记学习，从而成为大学学生群体的一员。所有在南洋理工大学就读学位课程的学生在开始学习之前都必须完成注册。

当学生完成学业并获得学位，或选择退学，或被学校终止学籍时，其注册学生的身份将终止。

南大新研究生的 __正式注册期__ 为 __2024年7月10日__ 至 __2024年7月16日__（新加坡时间上午10点至晚上10点）。

!!! warning "警告"

    如果您希望在南大就读，请务必完成在线注册。如果您 __没有__ 完成在线注册，您的名字将不会出现在南大的注册名册中。

## 登录注册系统

通过[注册系统](https://sso.wis.ntu.edu.sg/webexe88/owa/sso_login1.asp?t=2&p2=https://wis.ntu.edu.sg/webexe/owa/pgr$matriculation.freshmen_matriculate&extra=&pg=)登录在线注册门户。用户名是您的 __网络账号__，密码是您的 __网络密码__。例如，如果您的邮箱是 Zhangsan001@e.ntu.edu.sg，那么网络账号就是 __Zhangsan001__。然后选择 __student__ 作为您的域名。

请 __不要__ 使用您的邮箱登录此门户，这样是无法登录的。

<figure markdown="span">
    ![](online-matriculation/online-matriculation-portal.png){ width=640 .bordered .rounded }
</figure>

## 信息检查与注册

检查您的个人信息是否正确，然后点击 __我接受注册__ 以确认学校注册操作。点击后注册程序即 __结束__。

<figure markdown="span">
    ![](online-matriculation/online-matriculation-form.png){ width=800 .bordered .rounded }
</figure>

## 其他帮助

以下是您可能需要的关于个人信息更新（姓名、国籍、身份证号码、护照、地址等）、学生证和课程注册信息的其他帮助。

如果您不需要这些帮助，可以直接忽略本节。

### 个人信息更新

您可以通过[官方详情](https://entuedu.sharepoint.com/sites/Student/dept/sasd/oas/SitePages/Curriculum%20and%20Candidature/Update_personal_particular_graduate.aspx)查看官方个人信息更新文档。

我们请您确认上述显示的个人信息。请在 __GSLink__ 中更新任何变更。请确保您的个人信息在大学记录中始终 __保持最新__。由于地址或联系方式不正确或过时而导致的通信延迟或丢失，南大不承担责任。

已注册的学生应通过[个人信息变更](https://sso.wis.ntu.edu.sg/webexe88/owa/sso_login1.asp?t=3&p2=https://wis.ntu.edu.sg/pls/webexe/pgm$chgparticulars_main.main_pg&extra=&pg=)更新以下信息变更：

  - 地址
  - 联系电话
  - 个人电子邮箱
  - 紧急联系人详情

已注册的学生不仅需要通过[个人信息变更](https://sso.wis.ntu.edu.sg/webexe88/owa/sso_login1.asp?t=3&p2=https://wis.ntu.edu.sg/pls/webexe/pgm$chgparticulars_main.main_pg&extra=&pg=)更新变更，还需要在5个工作日内携带原始法律文件和学生证亲自到 OneStop@SAC 进行以下变更的验证：

!!! Danger inline end "重要提示"

    学生需要提前在[一站式服务门户](https://www.ntu.edu.sg/life-at-ntu/student-life/onestop)预约在线appointments。

  - 姓名
  - 国籍
  - 身份证
  - 护照

尚未注册的新生需通过发送电子邮件通知招生办公室更改其个人信息：

  - 授课型学生：admission_Coursework@ntu.edu.sg
  - 研究型学生：admission_Research@ntu.edu.sg

### 学生证领取

您可以通过[官方详情](https://www.ntu.edu.sg/admissions/matriculation/student_matriculation_card)查看官方学生证领取文档。

对于按时完成注册的学生，可以通过[领取时间表和地点](https://www.ntu.edu.sg/docs/default-source/office-of-academic-services/master_school-input_collection-schedule-for-pg-freshmen-admitting-in-s2-ay2023-24.pdf?sfvrsn=7e8d90b7_3)在学院领取学生证。具体领取时间将由学院另行通知。

<figure markdown="span">
    ![](online-matriculation/matriculation-card-pickup.png){ .bordered .rounded }
</figure>

对于在规定注册期后注册的学生和返校的国民服役人员，您将收到电子邮件通知（发送到您的南大电子邮箱），需要从 OneStop@SAC 而不是学院领取学生证，请提前在[一站式服务门户](https://www.ntu.edu.sg/life-at-ntu/student-life/onestop)登录并预约。

### 课程注册

您可以通过[官方详情](https://entuedu.sharepoint.com/sites/Student/dept/sasd/oas/SitePages/Course%20Registration/main.aspx)查看官方课程注册文档。

学生需要在课程注册期间注册他们希望在每个学期（学期/三学期）学习的课程。新生的第一学期课程将被预先分配。

学生可以通过[研究生课程注册系统](https://sso.wis.ntu.edu.sg/webexe88/owa/sso_redirect.asp?t=3&app=https://wish.wis.ntu.edu.sg/webexe/owa/PGR$SUBRS_WEB.main_pg)注册课程。可以查看[官方课程时间表](online-matriculation/PG Schedule for Course Registration (Sem 1 AY2024-2025).pdf)。课程选择的大致时间表如下：

| 日期和时间 | 详情 |
|-------------------------------------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| 2024年7月8日（11:00）至2024年7月16日（17:00） | __注册期 - [仅限在读学生]__ <br>在此期间，第2和第3优先级的学生将被列入等待名单，其注册的课程将在2024年7月22日确认 |
| 2024年8月7日（11:00至23:59） | __新土木工程硕士学生选择土木工程课程__ |
| 2024年8月8日（14:30）至2024年8月21日（23:59） | __添加/退选期 - [适用于在读和新生]__ <br>在此期间，第2和第3优先级的学生将被列入等待名单，其注册的课程将在2024年8月23日确认 |

所有学生按以下优先顺序注册课程：

  1. 授课型学生选择本专业提供的课程
  2. 授课型学生选择其他专业提供的课程
  3. 研究型学生

学生可以通过[学习方式选择](https://entuedu.sharepoint.com/sites/Student/dept/sasd/oas/SitePages/Course%20Registration/option-of-study.aspx)选择仅通过课程学习或通过课程学习和论文相结合的方式完成学业。

!!! warning "警告"

    所有学生的默认学习方式是 __课程学习 + 论文__。

本学期的课程表可在[研究生课程表](https://wish.wis.ntu.edu.sg/pls/webexe/pgr$subrs_timetable.mmenu)查看。您可以通过选择 __专业名称__ 或 __课程代码__ 搜索您的课程表。

<figure markdown="span">
    ![](online-matriculation/graduate-students-class-timetable.png){ .bordered .rounded }
</figure>