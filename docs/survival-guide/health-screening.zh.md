# 健康体检

新生必须通过体检才能满足新加坡移民与关卡局（ICA）规定的学生准证要求。

除非持有新加坡认可诊所的有效体检证明，否则南大学生必须在大学保健服务中心（富乐顿保健中心@NTU）进行体检。

本文将为您逐步介绍体检流程。

!!! info "强制性体检"

    体检是申请学生准证过程中的必要步骤之一。南大学生必须在新加坡完成体检，可以在大学保健服务中心（富乐顿保健中心@NTU）或其他认证的本地诊所进行。海外体检报告不予认可。

    如果您在预约体检时需要帮助，请直接致电 6793 6828 或发送电子邮件至 ntu@fullertonhealth.com 联系富乐顿保健中心@NTU。

## 账号注册

您需要在[富乐顿保健中心@NTU](https://bookappt.fullertonhealth.com/#/login)注册账号。

<figure markdown="span">
    ![](health-screening/sign-up.png){ width=300 .bordered .rounded }
</figure>

在注册表格中填写您的个人信息。国际学生请在_证件类型_部分选择 __护照号码__ 并填写您的护照号码。

在_公司_部分，输入 __NTU__ "并等待一两秒，直到系统自动提示正确选项 __NTU - NANYANG TECHNOLOGICAL UNIVERSITY__。选择该选项。

<figure markdown="span">
    ![](health-screening/sign-up-detail.png){ width=400 .bordered .rounded }
</figure>

## 登录

在登录界面，选择 __企业客户__ 并输入您注册的电话号码。请不要忘记包含 __国家代码__。初始密码是您的出生日期，格式为 __DDMMYYYY__。例如，如果您出生于 _2000年12月1日_，那么您的初始密码是`01122000`。

<figure markdown="span">
    ![](health-screening/login.png){ width=300 .bordered .rounded }
</figure>

## 预约体检

在 __预约体检__ 页面，您必须在 __预约类型__ 部分选择 __入学体检2024A__。如果您是国际学生，请 __预约上午时段__，以便进行 __X光检查__。

<figure markdown="span">
    ![](health-screening/book-appointment.png){ width=400 .bordered .rounded }
</figure>

!!! question "找不到_2024A_或_2024A_已约满"

    如果您在登录界面忘记选择 _企业客户_ 类型，将不会显示指定的 _入学体检2024A_ 选项。请注销并使用正确的选项重新登录。

    如果 _2024A_ 已约满，请尝试预约 _2024B_ 和 _2024C_。这两个选项是为解决 _2024A_ 预约过满而新增的。

继续完成预约表格。请注意，您应在 __附加说明__ 文本框中输入您的 __申请编号__（`Cxxxxxxx`）。

完成表格后，点击 __预约__ 按钮提交。确认邮件将发送到您注册的电子邮箱。

<figure markdown="span">
    ![](health-screening/book-appointment-2.png){ width=400 .bordered .rounded }
</figure>

## 修改预约

如果您想选择其他体检时段，可以在 __我的预约__ 页面修改现有预约。请注意，您必须在新预约时间至少48小时前完成修改操作。否则，您将需要重新预约。

<figure markdown="span">
    ![](health-screening/edit-appointment.png){ width=400 .bordered .rounded }
</figure>

## 体检流程

在预约当天，建议您提前半小时到达富乐顿保健中心@NTU - 请勿过早到达，否则可能会被要求不要加入队伍。

您应携带打印版的体检清单，即[ __MC1__（2022版）](health-screening/MC1.pdf)和[ __MC2__（2022版）](health-screening/MC2.pdf)。

在MC2 - 即 _体检报告_ - 中，您只需填写第一部分 _个人信息_，其余部分请留空。

<figure markdown="span">
    ![](health-screening/mc2.png){ width=400 .bordered .rounded }
</figure>

!!! warning "文件版本"

    您应检查MC表格是否符合最新的文件版本。

## 费用

在富乐顿保健中心@NTU进行体检需要缴纳费用。

| 学生类型 | 费用 |
|---------------------------------------------------------------------------------------------|:---:|
| 新加坡公民 <br/> 新加坡永久居民 <br/> 非全日制国际学生 | $32 |
| 全日制国际学生 | $50 |

建议您携带现金或有效的VISA / MasterCard / American Express信用卡支付这些费用。

电子发票将发送到您注册的电子邮箱。

## 结果

体检结果通常在完成所有检查项目后的 __3个工作日__ 内发布。以下 _实体_ 体检报告可在富乐顿保健中心@NTU前台领取：

* 体检报告（MC2）
* 胸部扫描结果
* HIV检测结果

领取结果后，请扫描结果/体检报告并上传至ICA网站的学生准证申请系统，以便继续申请流程。