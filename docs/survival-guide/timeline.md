# Timeline & Dates

!!! note

    Some of the timeline items do not apply to Singaporean or Permanent Resident students.

It can be challenging to start a chapter of your life at Nanyang Technological University, Singapore. To smoothen your experience with us, please follow this timeline.

## Academic Calendar

The EEE MSc student academic year at NTU is divided into two semesters:

- Semester 1: July to December
- Semester 2: January to May

Visit [NTU Academic Calendar](https://www.ntu.edu.sg/admissions/matriculation/academic-calendars) for detailed information about:

- Term Start Dates
- Examination Periods
- Vacation Schedules
- Key Academic Events

## Important Dates

Key dates for your matriculation process, visit [Important Dates for Graduate Students](https://www.ntu.edu.sg/admissions/graduate/gfreshmenguide/incoming-graduate-student-guide/important-dates) for:

- Student Pass Application (IPA -> STP)
- Complete Online Verification
- Health Screening
- Online Matriculation

!!! tip

    We recommend adding these important dates to your personal calendar to ensure you don't miss any deadlines.

## Timeline
[timeline(./docs/survival-guide/timeline/timeline.json)]
