# Online Matriculation

Matriculation is a formal process where a person registers for study at the University and thus becomes a member of the University student community. All students enrolling in NTU academic programmes that lead to the award of a degree must matriculate with the University before commencing their studies.

A person's status as a matriculated student will cease when he completes his study and is conferred his degree, or if he opts to withdraw from his study, or when his candidature is terminated by the University.

The __official matriculation period__ for NTU New Graduate Student is from __July 10, 2024__ to __July 16, 2024__ (Singapore Time 10.00 am to 10.00 pm).

!!! warning

    You are reminded to complete your matriculation online if you wish to enroll in NTU. Otherwise, your name will not be in NTU’s register if you __DO NOT__ matriculate online.

## Sign in Matriculation Portal

Sign in the online matriculation portal via [Matriculation Portal](https://sso.wis.ntu.edu.sg/webexe88/owa/sso_login1.asp?t=2&p2=https://wis.ntu.edu.sg/webexe/owa/pgr$matriculation.freshmen_matriculate&extra=&pg=). The username is your __Network Account__ and the password is your __Network Password__. For instance, The Network Account is __Zhangsan001__ if your email is Zhangsan001@e.ntu.edu.sg. Then select __student__ as your domain.

Please __DO NOT__ use your email to sign in this portal, it will not work.

<figure markdown="span">
    ![](online-matriculation/online-matriculation-portal.png){ width=640 .bordered .rounded }
</figure>

## Info Check & Matriculation

Check that your personal particulars is correct, and then click __I accept to matriculate__ to confirm the school registration operation. The matriculation process is __end after clicking__.

<figure markdown="span">
    ![](online-matriculation/online-matriculation-form.png){ width=800 .bordered .rounded }
</figure>

## Other Helps

Below you will find additional help you may need with personal particulars updates (name, nationality, ID number, passport, address, etc.), school card and course registration information.

If you do not need such helps, you can simply ignore this section.

### Personal Particulars Update

You can view the official personal particulars update documentation via [Official Detail](https://entuedu.sharepoint.com/sites/Student/dept/sasd/oas/SitePages/Curriculum%20and%20Candidature/Update_personal_particular_graduate.aspx).

We request that you confirm your personal particulars shown above. Please update any changes in __GSLink__. Please ensure that your personal particulars are always __up-to-date__ in the University's records. NTU cannot be accountable for delayed or lost correspondence due to incorrect or outdated addresses or contacts.

Matriculated students should __update changes to their personal particulars__  via [Change of Personal Particulars](https://sso.wis.ntu.edu.sg/webexe88/owa/sso_login1.asp?t=3&p2=https://wis.ntu.edu.sg/pls/webexe/pgm$chgparticulars_main.main_pg&extra=&pg=) for these following changes:

  - Addresses
  - Contact Numbers
  - Personal Email Address
  - Emergency Contact Details



Matriculated students not only need to update changes via [Change of Personal Particulars](https://sso.wis.ntu.edu.sg/webexe88/owa/sso_login1.asp?t=3&p2=https://wis.ntu.edu.sg/pls/webexe/pgm$chgparticulars_main.main_pg&extra=&pg=), but also need to produce the original legal document(s) and student matriculation card in person for verification at OneStop@SAC within 5 working days for these following changes:

!!! Danger inline end "Important Notice"

    Students require to make an online appointment in [One Stop Portal](https://www.ntu.edu.sg/life-at-ntu/student-life/onestop) ahead of their visit.

  - Name
  - Citizenship
  - NRIC
  - Passport


New students who have yet to be matriculated are to notify Office of Admissions of changes in their particulars via sending an email to:

  - Coursework students: admission_Coursework@ntu.edu.sg
  - Research students: admission_Research@ntu.edu.sg

### Matriculation Card Collection

You can view the official matriculation card collection documentation via [Official Detail](https://www.ntu.edu.sg/admissions/matriculation/student_matriculation_card).

For students who have completed their matriculation on time, matriculation cards will be available for pickup at the college via [Collection Schedule & Venue](https://www.ntu.edu.sg/docs/default-source/office-of-academic-services/master_school-input_collection-schedule-for-pg-freshmen-admitting-in-s2-ay2023-24.pdf?sfvrsn=7e8d90b7_3). Collection time to be notified by the college in the future.

<figure markdown="span">
    ![](online-matriculation/matriculation-card-pickup.png){ .bordered .rounded }
</figure>

For students who matriculate after the stipulated matriculation period and returning NSMen, you will receive email notification (sent to your NTU email account) to collect their card from OneStop@SAC instead of the School, please login and make an online appointment at the [One Stop portal](https://www.ntu.edu.sg/life-at-ntu/student-life/onestop) ahead of the visit.

### Course Registration

You can view the official course registration documentation via [Official Detail](https://entuedu.sharepoint.com/sites/Student/dept/sasd/oas/SitePages/Course%20Registration/main.aspx).

Students are to register for the courses they wish to read in each term (semester / trimester) within the course registration period. Freshmen will be pre-allocated courses for their first term of study.

Students could register for their courses via [Graudate Students Course Registration System](https://sso.wis.ntu.edu.sg/webexe88/owa/sso_redirect.asp?t=3&app=https://wish.wis.ntu.edu.sg/webexe/owa/PGR$SUBRS_WEB.main_pg). The [Official Course Schedule](online-matriculation/PG Schedule for Course Registration (Sem 1 AY2024-2025).pdf) is available for viewing. The approximate schedule for course selection is as follows:

| Date & Time                                           | Details                                                                                                                                                                                             |
|-------------------------------------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| 8 July 2024 (11:00 hrs) to 16 July 2024 (1700 hrs)    | __Registration Period - [ For Existing Students Only]__<br>During this period, students under order 2 and 3 will be on waiting list and their registered courses will be confirmed on 22 July 2024  |
| 7 August 2024 (1100 hrs to 2359 hrs)                  | __New M.Sc. (Civil Eng.) students select CEE courses__                                                                                                                                              |
| 8 August 2024 (1430 hrs) to 21 August 2024 (2359 hrs) | __Add/Drop Period - [For Existing AND New Students]__<br>During this period, students under order 2 and 3 will be on waiting list and their registered courses will be confirmed on 23 August 2024  |

All students register for courses in the following order of priority:

  1. Coursework Students choosing courses offered by their own programme of study
  2. Coursework Students choosing courses offered by other programme of study
  3. Research Students

Students could choose to complete their studies either through coursework alone or through a combination of coursework and dissertation via [Option of Study](https://entuedu.sharepoint.com/sites/Student/dept/sasd/oas/SitePages/Course%20Registration/option-of-study.aspx).

!!! warning

    The default option of study for all students is __Coursework + Dissertation__.

The Class Timetable for this semester is available at [Graduate Students Class Timetable](https://wish.wis.ntu.edu.sg/pls/webexe/pgr$subrs_timetable.mmenu). You could search for your course timetable by selecting the __programme name__ or __course code__.

<figure markdown="span">
    ![](online-matriculation/graduate-students-class-timetable.png){ .bordered .rounded }
</figure>
