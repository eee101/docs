# Applying for Student's Pass

!!! note

    This article applies for international students only. Singaporean or Permanent Residential students does not need a pass to study in NTU.

International students must possess a valid long-term pass, i.e., the __Student's Pass__ (STP) to stay in Singapore during their study at NTU.

<figure markdown="span">
    ![ICA logo](applying-for-stp/ica.png)
    <figurecaption>The Immigration & Checkpoint Authority of Singapore</figurecaption>
</figure>

To apply for an STP, you will need to file an application at the Singapore Immigration & Checkpoint Authority (ICA) [website](https://www.ica.gov.sg/). Upon approval, an In-Principle Approval (IPA) letter will be issued to you the first time as a single-entry visa to Singapore. A later process of STP formality will be performed under the __On-Site Enrollment__ (OSE) process so that you can obtain a valid STP. The OSE appointment will be announced via email; check both your application email and NTU email regularly.

Officers from ICA will step by NTU (normally on the top level of __Nanyang Auditorium__). Students who have _made appointments_ are eligible to head to the venue with their valid documents to complete the formality of their STP. Such documents include:

* Passport
* A 1-inch or 2-inch color-printed photo with a white background (regular non-matt material is acceptable, please refer to the [Photo Guidelines](https://www.ica.gov.sg/photo-guidelines))
* Digital version of the said photo, 400 x 514 pixels dimension
* Printed IPA letter (first page or double-sided are acceptable)

After the formality process, students will receive their digital STP within three business days.


!!! info "Misunderstanding in STP and IPA"

    In Singapore, an In-Principle Approval (IPA) letter is issued upon approval of all types of visa, as a "redeem letter" to the respective visa. It is not solely related to the STP. You are applying for a Student's Pass, not an IPA letter; it is totally not applicable per se.

    For instance, many students will apply for a Long-Term Visit Pass (LTVP) to stay in Singapore for job-seeking. During this process, students will be issued an IPA as well, upon LTVP approval.

    Many agents and social media posters mistakenly treat the application process as "apply for an IPA," which can cause confusion to the applicants.

Before that, you will need to check the information provided by NTU in the email entitled "__NTU Acceptance of Offer to Coursework Programme (xxxxxxxx)__" in _your <mark>application</mark> email account_.

The following screenshot shows an example email containing all the information required for STP application.

<figure markdown="span">
    ![](applying-for-stp/ntu-acceptance-of-offer-annotated.png){ .bordered .rounded .upper-fix }
    ![](applying-for-stp/ntu-acceptance-of-offer-annotated-2.png){ .bordered .rounded .lower-fix }
    <figurecaption>Screenshot of the _NTU Acceptance of Offer to Coursework Programme_ email</figurecaption>
</figure>

## Academic Particulars

The first table in this email screenshot lists your academic registration information, including your application number, matriculation number, date of birth, and the programme which you have accepted its offer.

Your __application number__ is composed of a letter __C__ ("C" for "Coursework"), a two-digit year indicator (e.g., "22" for the year "2022"), and a five-digit serial number (randomly assigned).

Your __matriculation number__ (a.k.a. student number) is your registered code number in NTU, and it will be printed on your matriculation card (a.k.a. student card). This number consists of a letter __G__ ("G" for "Graduate student"), a two-digit year indicator, and a five-digit serial number with a single letter suffix (randomly assigned).

Your __programme code__ is a fixed three-digit serial number. For example, the MSc in Communications Engineering programme code is [254].

## Application Guide

Paragraphs after the first table are guidelines to the online application process for your Student's Pass at ICA.

You must take note of the following items, as highlighted in the screenshot:

* date of programme commencement: e.g., 08 August 2022
* STP application number (SOLAR): e.g., TU-2022-C22xxxxx0000000
* your name (as reflected in passport)
* nationality: e.g., Chinese

The SOLAR number consists of three dash-connected components: a two-letter institutional code __TU__ ("TU" for "NTU"), a four-digit year indicator, and your application number followed by seven trailing zeros.

This number is strictly allocated with your registered information shown in the last table. Any difference or typo in this information during your ICA application may cause unsuccessful outcomes.

## Prepare Documentation

Before starting the application process, you must prepare the following documents, preferably in `jpg` or `pdf` format:

* Application number shown in your school letter, beginning with "TU-"
* Scanned image of your passport detail page
* Personal information as per listed in the school email
* Residential address, email, and contact in Singapore (e.g., school address, the address of your campus/rent housing, etc.)
* A recent passport-sized color photo of the applicant
* Valid VISA/MasterCard/American Express credit card to pay the fees incurred

## Process Briefing

This section guides you in the first steps for STP application.

Visit the ICA website at [https://www.ica.gov.sg/](https://www.ica.gov.sg/), and click on the __e-Services and Forms__ link, as marked in the following website screenshot.

<figure markdown="span">
    ![](applying-for-stp/ica-eform.png){ .bordered .rounded}
</figure>

Then scroll down to the __Long-Term Visit Pass and Student's Pass__ section, click the [Apply/Renew Student's Pass for Institutes of Higher Learning](https://www.ica.gov.sg/eservicesandforms/solar) link.

<figure markdown="span">
    ![](applying-for-stp/ica-eform-2.png){ .bordered .rounded}
</figure>

On the new page, you will be once again prompted to prepare valid documents to complete the application. Make sure you have these documents and click the [Foreign Student](https://eservices.ica.gov.sg/ipsolar/student/studentLogin) icon.

<figure markdown="span">
    ![](applying-for-stp/ica-stp-app.png){ .bordered .rounded width="300"}
</figure>

Type in the application number and your personal particulars as indicated in your NTU email, and click __Login__.

<figure markdown="span">
    ![](applying-for-stp/ica-solar.png){ .bordered .rounded }
</figure>

You will be asked to fill in a detailed form. Write in your personal information such as place of birth <mark>in accordance with that in the _said acceptance email_ and in your passport</mark>.

You can continue from here.

This guide page will be updated without notification.
