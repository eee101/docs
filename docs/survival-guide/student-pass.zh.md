# 申请学生准证

!!! note "注意"

    本文仅适用于国际学生。新加坡公民或永久居民在 NTU 就读无需申请准证。

国际学生必须持有有效的长期准证，即 __学生准证__（Student's Pass，简称STP）才能在 NTU 学习期间在新加坡居留。

<figure markdown="span">
    ![ICA标志](applying-for-stp/ica.png)
    <figurecaption>新加坡移民与关卡局</figurecaption>
</figure>

要申请学生准证，您需要在新加坡移民与关卡局（ICA）的[网站](https://www.ica.gov.sg/)提交申请。获得批准后，您将首次收到一份原则性批准信（IPA），作为入境新加坡的单次签证。之后将通过 __现场注册__（OSE）程序办理学生准证手续，以获取有效的学生准证。OSE预约将通过电子邮件通知；请定期查看您的申请邮箱和NTU邮箱。

ICA的工作人员将来到 NTU（通常在 __南洋礼堂__ 顶层）。已 _预约_ 的学生可携带有效文件前往现场完成学生准证手续。所需文件包括：

* 护照
* 1英寸或2英寸白底彩色照片（可使用普通非哑光材质，请参考[照片指南](https://www.ica.gov.sg/photo-guidelines)）
* 上述照片的数字版本，尺寸为400 x 514像素
* 打印的IPA信（第一页或双面打印均可）

完成手续后，学生将在三个工作日内收到数字版学生准证。

!!! info "关于学生准证和IPA的误解"

    在新加坡，原则性批准信（IPA）是所有类型签证获批后发放的"兑换信"。它不仅仅与学生准证有关。您申请的是学生准证，而不是IPA信；这两者本质上是不同的。

    例如，许多学生会申请长期访问准证（LTVP）以在新加坡求职。在这个过程中，学生在LTVP获批后也会收到IPA。

    许多中介和社交媒体帖子错误地将申请过程称为"申请IPA"，这可能会让申请人产生混淆。

在此之前，您需要在 _您的<mark>申请</mark>邮箱_ 中查看标题为"__NTU Acceptance of Offer to Coursework Programme (xxxxxxxx)__"的 NTU 邮件中提供的信息。

以下截图显示了一封包含申请学生准证所需所有信息的示例邮件。

<figure markdown="span">
    ![](applying-for-stp/ntu-acceptance-of-offer-annotated.png){ .bordered .rounded .upper-fix }
    ![](applying-for-stp/ntu-acceptance-of-offer-annotated-2.png){ .bordered .rounded .lower-fix }
    <figurecaption>_ NTU 授课型项目录取通知_邮件截图</figurecaption>
</figure>

## 学术信息

邮件截图中的第一个表格列出了您的学术注册信息，包括申请编号、学籍号、出生日期和您已接受录取的项目。

您的 __申请编号__ 由一个字母 __C__（"C"代表"授课型"）、两位年份指示符（如"22"代表"2022年"）和一个五位随机序列号组成。

您的 __学籍号__（又称学号）是您在 NTU 的注册代码，将印在您的学生证上。该号码由一个字母 __G__（"G"代表"研究生"）、两位年份指示符和一个带单字母后缀的五位随机序列号组成。

您的 __项目代码__ 是一个固定的三位序列号。例如，通信工程硕士项目代码是[254]。

## 申请指南

第一个表格后的段落是关于在ICA申请学生准证的在线申请流程指南。

您必须注意截图中突出显示的以下项目：

* 项目开始日期：例如，2022年8月8日
* 学生准证申请编号（SOLAR）：例如，TU-2022-C22xxxxx0000000
* 您的姓名（与护照一致）
* 国籍：例如，中国

SOLAR编号由三个用破折号连接的部分组成：两个字母的机构代码 __TU__（"TU"代表"NTU"）、四位年份指示符，以及您的申请编号后跟七个零。

该编号与最后一个表格中显示的注册信息严格对应。在ICA申请过程中，这些信息如有任何差异或错误都可能导致申请失败。

## 准备文件

在开始申请流程之前，您必须准备以下文件，最好是`jpg`或`pdf`格式：

* 学校信函中显示的以"TU-"开头的申请编号
* 护照个人信息页的扫描图像
* 学校邮件中列出的个人信息
* 在新加坡的住址、电子邮箱和联系方式（如学校地址、校内/租住地址等）
* 申请人近期的护照尺寸彩色照片
* 有效的VISA/MasterCard/American Express信用卡支付相关费用

## 流程简介

本节指导您完成学生准证申请的第一步。

访问ICA网站[https://www.ica.gov.sg/](https://www.ica.gov.sg/)，点击 __e-Services and Forms__（电子服务和表格）链接，如下图所示。

<figure markdown="span">
    ![](applying-for-stp/ica-eform.png){ .bordered .rounded}
</figure>

然后滚动到 __Long-Term Visit Pass and Student's Pass__（长期访问准证和学生准证）部分，点击[Apply/Renew Student's Pass for Institutes of Higher Learning](https://www.ica.gov.sg/eservicesandforms/solar)（高等院校学生准证申请/续签）链接。

<figure markdown="span">
    ![](applying-for-stp/ica-eform-2.png){ .bordered .rounded}
</figure>

在新页面上，系统会再次提示您准备有效文件以完成申请。确保您已准备好这些文件，然后点击[Foreign Student](https://eservices.ica.gov.sg/ipsolar/student/studentLogin)（外国学生）图标。

<figure markdown="span">
    ![](applying-for-stp/ica-stp-app.png){ .bordered .rounded width="300"}
</figure>

输入 NTU 邮件中指示的申请编号和个人信息，然后点击 __Login__（登录）。

<figure markdown="span">
    ![](applying-for-stp/ica-solar.png){ .bordered .rounded }
</figure>

系统将要求您填写详细表格。请填写个人信息，如出生地点，<mark>需与 _上述录取通知邮件_ 和护照中的信息一致</mark>。

您可以从这里继续。

本指南页面将不定期更新。