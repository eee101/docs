# 时间表与重要日期

!!! note "注意"

    部分时间节点不适用于新加坡公民或永久居民学生。

在新加坡南洋理工大学开启人生新篇章可能会有些挑战。为了让你的入学体验更顺畅，请按照以下时间表进行准备。

## NTU 校历

南洋理工大学电气与电子工程学院硕士课程的学年分为两个学期：

- 第一学期：7月至12月
- 第二学期：1月至5月

访问 [NTU 校历](https://www.ntu.edu.sg/admissions/matriculation/academic-calendars) 获取以下详细信息：

- 学期开始日期
- 考试时间
- 假期安排
- 重要学术活动

## 重要日期

有关入学程序的重要日期，请访问 [研究生重要日期](https://www.ntu.edu.sg/admissions/graduate/gfreshmenguide/incoming-graduate-student-guide/important-dates) 了解：

- 学生准证申请（IPA -> STP）
- 完成在线验证
- 体检
- 在线注册

!!! tip "小贴士"

    建议将这些重要日期添加到你的个人日历中，以确保不会错过任何截止日期。

## 时间线
[timeline(./docs/survival-guide/timeline/timeline.zh.json)]
