# Calculator Registration

## Overview

For examinations at NTU, only registered calculators are permitted. To ensure examination fairness, all students must complete the calculator registration process before using calculators in examinations.

For the latest information about approved calculators and registration dates, please visit the [Official Calculator Registration Page](https://www.ntu.edu.sg/computing/admissions/undergraduate-programmes/list-of-approved-calculators-registration-dates).

## Approved Calculator Models

Please refer to the [approved calculator models list](./calculator-%20registration/approved-calculators.pdf) for the complete and up-to-date information. Only calculator models listed in the official document are permitted for examinations.

!!! warning "Important Notice"

    - Only calculator models listed in the official document are permitted for examinations
    - Calculators must be in their original condition without modifications
    - No markings or writings are allowed on calculator casings

## Registration Venue and Time

- Location: EEE MSc Offica (S2.1-B2-19)

| Period | Session 1 | Session 2 | Session 3 |
|--------|-----------|-----------|-----------|
| Semester 1 | Sep 09-13<br>(10 AM - 4 PM) | Oct 21-25<br>(10 AM - 4 PM) | Nov 18 - Dec 06<br>(8:30 AM - 5 PM) |
| Semester 2 | Feb 10-14<br>(10 AM - 4 PM) | Mar 24-28<br>(10 AM - 4 PM) | Apr 21 - May 09<br>(8:30 AM - 5 PM) |
| Special Term | TBC | - | - |

## FAQ

??? question "What should I do if my calculator registration sticker falls off?"

    Please visit EEE MSc Office immediately for re-registration. It's recommended to check your calculator sticker condition before examinations.

??? question "Can I register multiple calculators?"

    Yes, but each calculator must be from the approved model list.

??? question "What if I need to replace my calculator?"

    After purchasing a new calculator, you'll need to complete the registration process again. It's recommended to keep your old calculator as a backup.

??? question "What should I do if my calculator is not on the approved model list?"  

    Fill out the new model application form, submit it to the relevant authority for review, and wait for approval. Once approved, you can proceed with the calculator registration and labeling process. If you need further assistance, contact the exam administration office.