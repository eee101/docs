# 接受录取 Offer

恭喜！你已被南洋理工大学电气与电子工程学院录取为全日制硕士研究生。现在你可以按照本指南接受录取并开启一段激动人心的旅程。

!!! note "注意"

    只有在收到标题为 _南洋理工大学(NTU)授课型项目录取通知(C0000000)_ 的邮件后，才能开始以下操作。

## 下载录取通知书

首先你需要 __下载正式的录取通知书__。这份文件包含了你的录取信息和后续需要遵循的步骤。

你可以在[录取系统](https://wis.ntu.edu.sg/webexe/owa/pga$cw_eoffer_display.main)下载录取通知书。如果你没有立即在系统中看到录取通知，这是正常的。系统更新可能需要 __1~3个工作日__。

登录录取系统后，点击 __红色链接__ (D:\EOFFER_C0000000_000.pdf)下载你的录取通知书。

<figure markdown="span">
    ![](accepting-your-offer/download-offer-letter.png){ .bordered .rounded }
</figure>

## 接受录取

在收到标题为 __南洋理工大学(NTU)授课型项目录取通知(C0000000)__ 的邮件后，你可以接受录取。请务必在邮件中注明的 __截止日期前__ 完成，否则将被视为自动放弃录取。

在[接受录取系统](https://wis.ntu.edu.sg/webexe/owa/pgr$acceptance_offer.main)中 __接受录取__。

<figure markdown="span">
    ![](accepting-your-offer/accept-offer.png){ .bordered .rounded }
</figure>

接受录取后，你将收到一封 __确认邮件__，同时也可以通过[录取状态查询系统](https://wis.ntu.edu.sg/webexe/owa/pgr$adm_acceptform.main) __查看你的录取状态__。

<figure markdown="span">
    ![](accepting-your-offer/check-offer-status.png){ .bordered .rounded }
</figure>

## 缴纳录取定金

为确保你的入学名额，你需要支付 __5,000新元__（含消费税）的不可退还录取定金来确认接受录取。这笔定金将在项目第一学期注册时用于抵扣学费。

!!! warning "警告"

    请在 __收到定金缴纳邮件后__ 支付录取定金，并且 __不要__ 错过截止日期。

你可以通过[在线支付系统](https://ntuadminonestop.service-now.com/ntusp?id=ntu_adhoc_payment)缴纳录取定金。选择相应的项目名称并点击支付按钮继续。例如，如果你被录取到 __计算机控制与自动化__ 硕士项目，请选择 __Deposit-EEE MSC in Computer Control & Automation programmes__。

<figure markdown="span">
    ![](accepting-your-offer/deposit-payment-entrance.png){ .bordered .rounded }
</figure>

填写在线支付表单，选择 PayNow 或 Visa / Master 卡支付。请注意在 __Reference ID__ 处填写你的 __申请编号__，切勿填错。

<figure markdown="span">
    ![](accepting-your-offer/deposit-payment-form.png){ .bordered .rounded }
</figure>

请保留收到的 __收据副本__，以备日后核验。

<figure markdown="span">
    ![](accepting-your-offer/receipt.png){ width=500 .bordered .rounded }
</figure>

## 阅读欢迎信

在学院决定录取你后，你还将收到一封欢迎信。这封信包含了你的项目的重要信息，包括开学日期、迎新会和其他重要细节。

<figure markdown="span">
    ![](accepting-your-offer/welcome-letter.png){ width=800 .bordered .rounded }
</figure>