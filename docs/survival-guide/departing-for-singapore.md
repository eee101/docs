# Departing for Singapore

To ensure a smooth travel experience for everyone, we have carefully prepared a detailed procedure for your reference. Please review and follow the steps outlined below to make your journey as seamless as possible.

<div class="bordered rounded" style="max-width: 800px">
  ``` mermaid
  sequenceDiagram
    autonumber
    Before Departing ->> Before Departing: Prepare Passport & IPA letter
    Before Departing ->> Before Departing: Submit Online SG Arrival Card
    Before Departing ->> Arrival at Airport: Take airplane to Singapore
    Arrival at Airport ->> Arrival at Airport: Automated clearance by Passport
    Arrival at Airport ->> Arrival at Airport: Purchase local SIM card
    Arrival at Airport ->> Arrival at NTU: Take transportation to NTU
    Arrival at NTU ->> Arrival at NTU: Check in (campus / off-campus)
    Arrival at NTU ->> Arrival at NTU: Health screening at Fullerton
    Arrival at NTU ->> Arrival at NTU: Complete STP formalities (OSE)
    Arrival at NTU ->> Arrival at NTU: Apply for bank card
  ```
</div>

## Before Departing

You should prepare the following documents before your departure. The first two are __MANDATORY__ as they are required for entry into Singapore.

!!! Tip inline end "Optional Documents"

    Any other optional documents could be printed __arfter you arrive__.

  - [x] Passport
  - [x] In-Principle Approval (IPA) letter
  - [ ] Passport-sized photograph (1)
    { .annotate }

     1. - 35mm wide by 45mm high<br>
        - Taken within the last 3 months<br>
        - White background with a matt finish<br>
        - Full face and without headgear

  - [ ] [Health Screening Form (MC1)](health-screening/MC1.pdf)
  - [ ] [Medical Examination Report (MC2, for STP)](health-screening/MC2.pdf)
  - [ ] [Consent to Terms and Conditions of STP (for STP)](departing-for-singapore/terms-conditions-stp.pdf)


Before you depart, you need to complete the __Online SG Arrival Card__ within __3 days (72 hours)__ before arrival. You can access the online form via [SG Arrival Card Portal](https://eservices.ica.gov.sg/sgarrivalcard/).

If you are eligible for entry intoSingapore, you will receive an __electronic pass (e-Pass)__ sent to you via this email address after clearing Singapore immigration. Please note that the email address is automatically set in uppercase format for clarity purpose.

## Arrival at Airport

Upon arrival at Singapore's Changi Airport, you need to complete these two tasks:

  - Automated Clearance
  - Purchase SIM Card

__Automated Clearance__ is a self-service option that allows visitors to quickly enter Singapore. By completing the __Online SG Arrival Card__, you can enjoy automated clearance simply by tapping your passport.

<figure markdown="span">
    ![](departing-for-singapore/automated-clearance.png){ width=500 .bordered .rounded }
</figure>

__Purchase SIM Card__ is essential for you to make sure you can use the local apps and services. You can purchase a SIM card in advance from platforms like [Pelago](https://www.pelago.com), [Klook](https://www.klook.com), [Ctrip](https://ctrip.com), or [Fliggy](https://www.fliggy.com) and redeem it at the airport.

For instance, you can get a __discount__ on SIM card bought via Pelago if you purchase a Singapore Airlines ticket or new member of Pelago.

Alternatively, you can buy one at [7-Eleven]() or [Cheers]() after you arrive.

<figure markdown="span">
    ![](departing-for-singapore/tourist-sim-card.png){ width=600 .bordered .rounded }
</figure>

After completing the above steps, you could take cab, bus & metro or use EEE pickup service to reach NTU.

## Arrival at NTU

Once you arrive at NTU, complete the following steps to settle in:

  - Check in Accommodation
  - Health Screening
  - STP Formalities (OSE)
  - Bank Cards Application

### Check in Accommodation

__Check in Accommodation__ is the first priority after arriving at Singapore. During the check-in process, carefully inspect the condition of the room and the functionality of the furniture. Take photos to document any existing issues to ensure a smooth process when you move out.

### Health Screening

__Health Screening__ is mandatory for all new students upon arrival. For international students, there are two parts to the process. The results from the MC2, will be used for the Student Pass formalities.

For more details, please visit [Health Screening Article](health-screening.md).

### STP Formalities (OSE)

__STP Formalities (OSE)__ is responsible for facilitating the exchange of your In-Principle Approval (IPA) for the Student Pass.

__The Student Pass__ is essential for long-term stay in Singapore. It allows you to stay beyond the 30-day limit for tourists.

For more details, please visit [Applying for Student's Pass Article](student-pass.md).

### Bank Cards Application

You can set up a local bank account to facilitate transferring funds from your home country to Singapore after obtaining your Student Pass.

Common local banks in Singapore include DBS, OCBC, and UOB. Choose the one you like the most and apply for the debit card.

For more details, please visit [Bank Cards Application Article]().
