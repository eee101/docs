# 常用链接

## 入学与注册

<div class="grid cards" markdown>

- :material-calendar-clock: __重要时间表__
    
    新生入学重要日期和截止时间
    
    [:octicons-arrow-right-24: 查看时间表](https://www.ntu.edu.sg/admissions/graduate/gfreshmenguide/important-dates){:target="_blank"}

- :material-file-document: __录取通知书__
    
    下载录取通知书
    
    [:octicons-arrow-right-24: 访问系统](https://wis.ntu.edu.sg/webexe/owa/pga$cw_eoffer_display.main){:target="_blank"}

<!-- - :material-cash: __缴纳定金__
    
    缴纳入学定金
    
    [:octicons-arrow-right-24: 在线支付](https://ntuadminonestop.service-now.com/ntusp?id=ntu_adhoc_payment){:target="_blank"} -->

- :material-check-circle: __录取确认__
    
    查看录取确认状态
    
    [:octicons-arrow-right-24: 查看状态](https://wis.ntu.edu.sg/webexe/owa/pgr$adm_acceptform.main){:target="_blank"}

- :material-hospital: __体检预约__
    
    预约必需的入学体检
    
    [:octicons-arrow-right-24: 预约体检](https://bookappt.fullertonhealth.com/#/login){:target="_blank"}

- :material-home: __住宿服务__
    
    申请校园住宿
    
    [:octicons-arrow-right-24: 申请系统](https://www.ntu.edu.sg/life-at-ntu/accommodation){:target="_blank"}

- :material-passport: __学生准证__
    
    申请学生准证
    
    [:octicons-arrow-right-24: 立即申请](https://eservices.ica.gov.sg/solar/index.xhtml){:target="_blank"}

- :material-check-decagram: __在线验证__
    
    完成在线身份验证
    
    [:octicons-arrow-right-24: 开始验证](https://venus.wis.ntu.edu.sg/GEVER/GEVER_StudentStartPage.aspx){:target="_blank"}

- :material-school-outline: __在线注册__
    
    完成入学注册流程
    
    [:octicons-arrow-right-24: 开始注册](https://sso.wis.ntu.edu.sg/webexe88/owa/sso_login1.asp?t=2&p2=https://wis.ntu.edu.sg/webexe/owa/pgr$matriculation.freshmen_matriculate&extra=&pg=){:target="_blank"}

</div>

## 学术服务

<div class="grid cards" markdown>

- :material-book-education: __硕士项目指南__
    
    硕士生必读信息
    
    [:octicons-arrow-right-24: 查看指南](https://entuedu.sharepoint.com/sites/Student/cs/eee/SitePages/Graduate/M.Sc.%20Programme/Master%20of%20Science%20(MSc)%20Programme/MasterofScience(MSc)Programme.aspx){:target="_blank"}

- :material-calendar: __学术日历__
    
    学年时间表和重要日期
    
    [:octicons-arrow-right-24: 查看日历](https://www.ntu.edu.sg/admissions/matriculation/academic-calendars#Content_C007_Col00){:target="_blank"}

- :material-school: __NTU Learn__
    
    课程资料和作业管理平台
    
    [:octicons-arrow-right-24: 访问平台](https://ntulearn.ntu.edu.sg){:target="_blank"}

- :material-timetable: __GSLink 研究生系统__
    
    GSLink 课程注册、考试时间表和成绩查询
    
    [:octicons-arrow-right-24: 访问系统](https://sso.wis.ntu.edu.sg/links/pgstudent.html){:target="_blank"}

- :material-library: __图书馆__
    
    图书馆资源和设施
    
    [:octicons-arrow-right-24: 图书馆主页](https://www.ntu.edu.sg/library){:target="_blank"}

- :material-desk: __图书馆设施__
    
    预约讨论室和学习空间
    
    [:octicons-arrow-right-24: 预约系统](https://libcalendar.ntu.edu.sg/){:target="_blank"}

- :material-database: __电子数据库__
    
    访问NTU电子数据库
    
    [:octicons-arrow-right-24: 电子图书馆](https://libguides.ntu.edu.sg/az.php){:target="_blank"}

- :material-file-document-edit: __论文时间表__
    
    硕士论文流程和提交时间表
    
    [:octicons-arrow-right-24: 查看流程](https://entuedu.sharepoint.com/sites/Student/cs/eee/Shared%20Documents/Forms/AllItems.aspx?id=%2Fsites%2FStudent%2Fcs%2Feee%2FShared%20Documents%2FGraduate%2FM%2ESc%2E%20Programme%2FMaster%20of%20Science%20%28MSc%29%20Programme%2FImportant%20Links%2FDissertation%2FMSc%20Dissertation%20Process%20Flow%5Fupdated%2Epdf&parent=%2Fsites%2FStudent%2Fcs%2Feee%2FShared%20Documents%2FGraduate%2FM%2ESc%2E%20Programme%2FMaster%20of%20Science%20%28MSc%29%20Programme%2FImportant%20Links%2FDissertation){:target="_blank"}
    
</div>

## IT服务

<div class="grid cards" markdown>

- :material-account-plus: __网络账号注册__
    
    注册NTU网络账号
    
    [:octicons-arrow-right-24: 注册账号](https://wis.ntu.edu.sg/webexe/owa/account_creation.logon1){:target="_blank"}

- :material-key: __密码重置__
    
    重置网络和Microsoft 365密码
    
    [:octicons-arrow-right-24: 重置密码](https://pwd.ntu.edu.sg/){:target="_blank"}

- :material-microsoft-office: __Microsoft 365__
    
    访问NTU邮箱和Microsoft服务
    
    [:octicons-arrow-right-24: 登录](https://www.outlook.com/e.ntu.edu.sg){:target="_blank"}

</div>

## 校园服务

<div class="grid cards" markdown>

- :material-food: __餐饮指南__
    
    校园餐饮场所目录
    
    [:octicons-arrow-right-24: 餐饮目录](https://www.ntu.edu.sg/life-at-ntu/leisure-and-dining/general-directory){:target="_blank"}

- :material-bus: __校园巴士__
    
    校园巴士路线和时刻表
    
    [:octicons-arrow-right-24: 巴士信息](https://www.ntu.edu.sg/about-us/visiting-ntu/internal-campus-shuttle){:target="_blank"}

- :material-medical-bag: __大学医疗服务__
    
    医疗服务和健康信息
    
    [:octicons-arrow-right-24: 医疗服务](https://www.ntu.edu.sg/life-at-ntu/health-and-safety/services/health-care){:target="_blank"}

- :material-dumbbell: __体育与娱乐__
    
    体育设施和健身项目
    
    [:octicons-arrow-right-24: 预约系统](https://ntu.facilitiesbooking.com/login.aspx){:target="_blank"}
    
- :material-account-group: __学生组织__
    
    学生社团和社区目录
    
    [:octicons-arrow-right-24: 查看组织](https://www.ntu.edu.sg/life-at-ntu/student-life/student-activities-and-engagement/clubs-groups-societies){:target="_blank"}

- :material-cash-multiple: __勤工助学系统__
    
    提交和跟踪报销申请
    
    [:octicons-arrow-right-24: WSS系统](https://apps.ntu.edu.sg/WSSClaims_Student/){:target="_blank"}

</div>