# Useful Links

## Admission & Matriculation

<div class="grid cards" markdown>

- :material-calendar-clock: __Important Timeline__
    
    Key dates and deadlines for new students
    
    [:octicons-arrow-right-24: View Timeline](https://www.ntu.edu.sg/admissions/graduate/gfreshmenguide/important-dates){:target="_blank"}

- :material-file-document: __Offer Download__
    
    Download your admission offer letter
    
    [:octicons-arrow-right-24: Access Portal](https://wis.ntu.edu.sg/webexe/owa/pga$cw_eoffer_display.main){:target="_blank"}

<!-- - :material-cash: __Payment of Deposit__
    
    Make payment for your admission deposit
    
    [:octicons-arrow-right-24: Make Payment](https://ntuadminonestop.service-now.com/ntusp?id=ntu_adhoc_payment){:target="_blank"} -->

- :material-check-circle: __Offer Acceptance Status__
    
    Check your offer acceptance status
    
    [:octicons-arrow-right-24: Check Status](https://wis.ntu.edu.sg/webexe/owa/pgr$adm_acceptform.main){:target="_blank"}

- :material-hospital: __Health Screening Booking__
    
    Book your mandatory health screening appointment
    
    [:octicons-arrow-right-24: Book Appointment](https://bookappt.fullertonhealth.com/#/login){:target="_blank"}

- :material-home: __Housing Services__
    
    Apply for campus accommodation
    
    [:octicons-arrow-right-24: Application Portal](https://www.ntu.edu.sg/life-at-ntu/accommodation){:target="_blank"}

- :material-passport: __Student Pass Application__
    
    Apply for your Student Pass
    
    [:octicons-arrow-right-24: Apply Now](https://eservices.ica.gov.sg/solar/index.xhtml){:target="_blank"}

- :material-check-decagram: __Online Verification__
    
    Complete your online verification
    
    [:octicons-arrow-right-24: Verify Now](https://venus.wis.ntu.edu.sg/GEVER/GEVER_StudentStartPage.aspx){:target="_blank"}

- :material-school-outline: __Online Matriculation__
    
    Complete your matriculation process
    
    [:octicons-arrow-right-24: Matriculate](https://sso.wis.ntu.edu.sg/webexe88/owa/sso_login1.asp?t=2&p2=https://wis.ntu.edu.sg/webexe/owa/pgr$matriculation.freshmen_matriculate&extra=&pg=){:target="_blank"}



</div>

## Academic

<div class="grid cards" markdown>

- :material-book-education: __MSc Programme Guide__
    
    Essential information for MSc students
    
    [:octicons-arrow-right-24: View Guide](https://entuedu.sharepoint.com/sites/Student/cs/eee/SitePages/Graduate/M.Sc.%20Programme/Master%20of%20Science%20(MSc)%20Programme/MasterofScience(MSc)Programme.aspx){:target="_blank"}

- :material-calendar: __Academic Calendars__
    
    Academic year schedule and important dates
    
    [:octicons-arrow-right-24: View Calendar](https://www.ntu.edu.sg/admissions/matriculation/academic-calendars#Content_C007_Col00){:target="_blank"}

- :material-school: __NTU Learn__
    
    Your one-stop platform for course materials and assignments
    
    [:octicons-arrow-right-24: View NTU Learn](https://ntulearn.ntu.edu.sg){:target="_blank"}

- :material-timetable: __Graduate Students Link__
    
    Course registration, exam timetable, and results
    
    [:octicons-arrow-right-24: View GSLink](https://sso.wis.ntu.edu.sg/links/pgstudent.html){:target="_blank"}

- :material-library: __NTU Library__
    
    Access to library resources and facilities
    
    [:octicons-arrow-right-24: Library Portal](https://www.ntu.edu.sg/library){:target="_blank"}

- :material-desk: __Library Facilities__
    
    Book discussion rooms and study spaces
    
    [:octicons-arrow-right-24: Book Facilities](https://libcalendar.ntu.edu.sg/){:target="_blank"}

- :material-database: __E-Resources__
    
    Access NTU electronic databases
    
    [:octicons-arrow-right-24: E-Library](https://libguides.ntu.edu.sg/az.php){:target="_blank"}

- :material-file-document-edit: __Dissertation Timeline__
    
    MSc dissertation process flow and submission timeline
    
    [:octicons-arrow-right-24: View Process](https://entuedu.sharepoint.com/sites/Student/cs/eee/Shared%20Documents/Forms/AllItems.aspx?id=%2Fsites%2FStudent%2Fcs%2Feee%2FShared%20Documents%2FGraduate%2FM%2ESc%2E%20Programme%2FMaster%20of%20Science%20%28MSc%29%20Programme%2FImportant%20Links%2FDissertation%2FMSc%20Dissertation%20Process%20Flow%5Fupdated%2Epdf&parent=%2Fsites%2FStudent%2Fcs%2Feee%2FShared%20Documents%2FGraduate%2FM%2ESc%2E%20Programme%2FMaster%20of%20Science%20%28MSc%29%20Programme%2FImportant%20Links%2FDissertation){:target="_blank"}
    
</div>

## IT Services

<div class="grid cards" markdown>

- :material-account-plus: __Network Account Registration__
    
    Register your NTU network account
    
    [:octicons-arrow-right-24: Register Account](https://wis.ntu.edu.sg/webexe/owa/account_creation.logon1){:target="_blank"}

- :material-key: __Password Reset__
    
    Reset your Network & Microsoft 365 password
    
    [:octicons-arrow-right-24: Reset Password](https://pwd.ntu.edu.sg/){:target="_blank"}

- :material-microsoft-office: __Microsoft 365__
    
    Access your NTU email and Microsoft services
    
    [:octicons-arrow-right-24: Login](https://www.outlook.com/e.ntu.edu.sg){:target="_blank"}

</div>

## Campus Services

<div class="grid cards" markdown>

- :material-food: __NTU F&B Directory__
    
    Find food and beverage outlets across campus
    
    [:octicons-arrow-right-24: F&B Directory](https://www.ntu.edu.sg/life-at-ntu/leisure-and-dining/general-directory){:target="_blank"}

- :material-bus: __NTU Shuttle Bus__
    
    NTU Campus shuttle bus routes and schedules
    
    [:octicons-arrow-right-24: Shuttle Bus Plan](https://www.ntu.edu.sg/about-us/visiting-ntu/internal-campus-shuttle){:target="_blank"}

- :material-medical-bag: __University Health Service__
    
    Medical services and health information
    
    [:octicons-arrow-right-24: Health Service](https://www.ntu.edu.sg/life-at-ntu/health-and-safety/services/health-care){:target="_blank"}

- :material-dumbbell: __Sports & Recreation__
    
    Sports facilities and fitness programs
    
    [:octicons-arrow-right-24: Booking Portal](https://ntu.facilitiesbooking.com/login.aspx){:target="_blank"}
    
- :material-account-group: __Student Organizations__
    
    Directory of student clubs and societies
    
    [:octicons-arrow-right-24: View Organizations](https://www.ntu.edu.sg/life-at-ntu/student-life/student-activities-and-engagement/clubs-groups-societies){:target="_blank"}

- :material-cash-multiple: __Work Study Scheme__
    
    Submit and track your claims and reimbursements
    
    [:octicons-arrow-right-24: WSS Portal](https://apps.ntu.edu.sg/WSSClaims_Student/){:target="_blank"}

</div>