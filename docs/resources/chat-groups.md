# Chat Groups

To better communicate each other, we have established a few chat groups on platforms such as WeChat and WhatsApp.
        
## Platforms

Choosing a suitable platform is essential since there aren't many chat apps literally _globally available_. As a matter of fact, international operations for almost all major chat apps are restricted or regulated by local authorities. You may alternatively use multiple platforms if you travel globally often.

Installation of the above mentioned apps is straightforward:

=== ":material-wechat:{ .lg } WeChat"

    ``` markdown title="WeChat Web Site"
    https://www.wechat.com/en/
    ```

    <div class="result" markdown>

    ![WeChat web page][WeChat web page]{ .rounded }

    WeChat is a super-app developed by Tencent in China, offering messaging, social networking, e-commerce, and mobile payment services in one platform. It's widely used with over a billion monthly active users globally.
    
    Find the [app][WeChat web site] that suits your operating system.

    </div>

=== ":material-whatsapp:{ .lg } WhatsApp"

    ``` markdown title="WhatsApp Web Site"
    https://www.whatsapp.com/
    ```

    <div class="result" markdown>

    ![WhatsApp web page][WhatsApp web page]{ .rounded }

    WhatsApp is a messaging app for text, voice, and video communication, available on multiple platforms. It offers end-to-end encryption for privacy and is widely used globally.
    
    Click [:material-download-circle:][WhatsApp web site] to download.

    [WeChat web site]: https://www.wechat.com/en/
    [WhatsApp web site]: https://www.whatsapp.com/
    [WeChat web page]: chat-groups/wechat-website-screenshot.png
    [WhatsApp web page]: chat-groups/whatsapp-website-screenshot.png

    </div>


You will need a valid phone number to register your user account on both platforms. Typically, SMS is used to receive one-time prompt code. A valid internet connection is required during this process.

??? warning "Access restrictions"

    There are a few access restrictions when using both WeChat and WhatsApp.

    It is difficult to connect WhatsApp servers from within mainland China due to the US sanctions and data policy, combined with Chinese internet restrictions. However, this is resolvable by using a legal VPN.

    The Indian government bannd WeChat in the year 2020 citing national security concerns. Therefore, it is impossible to access WeChat legally from India.

## WeChat Group

A WeChat group can host up to 500 participants. We will announce more groups if the old ones are filled.

Scan the following QR to join in our Group.

![WeChat group](chat-groups/wechat-group-2025.png){ load=lazy width="300" }

Due to policy restrictions, WeChat automatically enables verification when the number of group members exceeds 200.
Please add the following administrators and ask them to invite you in if you are unable to join in.

=== "ryan"

    ![][ryan]{ width="300" .rounded }


    [ryan]: chat-groups/admin-ryan.jpg

=== "echo"

    ![][echo]{ width="300" .rounded }


    [echo]: chat-groups/admin-echo.jpg


## WhatsApp Groups

WhatsApp groups can host up to 256 participants each. We have set up a WhatsApp community with five channels for each MSc program.

Click the links to join in respective WhatsApp groups for our Master of Science degree programs.

<div class="grid cards" markdown>

-   :material-satellite-uplink:{ .lg .middle } __Communications Engineering__

    ---

    Covers various important topics in telecommunications, RF engineering and wireless communications.

    [:octicons-arrow-right-24: Join CME channel][CME]

-   :material-robot:{ .lg .middle } __Computer Control & Automation__

    ---

    Provides advanced practical tools for computer-based control and automation systems.

    [:octicons-arrow-right-24: Join CCA channel][CCA]

-   :simple-electron:{ .lg .middle } __Electronics__

    ---

    Covers IC design, microelectronics fabrication and manufacture of electronic and photonic products.

    [:octicons-arrow-right-24: Join ET channel][ET]

-   :material-wind-power:{ .lg .middle } __Power Engineering__

    ---

    Designed for graduates who are practicing engineers, R&D managers, power system designers or industry planners.

    [:octicons-arrow-right-24: Join PE channel][PE]

-   :material-sine-wave:{ .lg .middle } __Signal Processing & Machine Learning__

    ---

    Targets engineers, designers, data scientists, R&D managers, and industry planners interested in DSP and AI.

    [:octicons-arrow-right-24: Join SPML channel][SPML]

    [CME]: https://chat.whatsapp.com/JtSWdnvkuFpDDUlMDfrN85
    [CCA]: https://chat.whatsapp.com/KTFePfashAx0jkFUvNJTRa
    [ET]: https://chat.whatsapp.com/DRjb461KM1eIvzWFAEvDxN
    [PE]: https://chat.whatsapp.com/FGtp2jLFPRFC7JoHVojCkO
    [SPML]: https://chat.whatsapp.com/LW7Bo2tKssGJ9qQC1HyOuj

</div>

!!! tip "Be considerate"

    We have limited vacancies in each channel. Please join in __only the channel of your program__.