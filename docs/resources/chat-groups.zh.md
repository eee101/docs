# 聊天群组

为了更好地相互交流，我们在微信和 WhatsApp 等平台上建立了多个聊天群组。
        
## 平台选择

选择合适的平台很重要，因为很少有聊天应用程序是真正的_全球可用_。实际上，几乎所有主要聊天应用的国际业务都受到当地政府的限制或管理。如果您经常全球旅行，可以考虑同时使用多个平台。

以下应用的安装过程都很简单：

=== ":material-wechat:{ .lg } 微信"

    ``` markdown title="微信官网"
    https://www.wechat.com/en/
    ```

    <div class="result" markdown>

    ![WeChat web page][WeChat web page]{ .rounded }

    微信是由腾讯开发的超级应用，在一个平台上提供即时通讯、社交网络、电子商务和移动支付服务。它在全球拥有超过十亿的月活跃用户。
    
    选择适合您操作系统的[应用程序][WeChat web site]下载。

    </div>

=== ":material-whatsapp:{ .lg } WhatsApp"

    ``` markdown title="WhatsApp 官网"
    https://www.whatsapp.com/
    ```

    <div class="result" markdown>

    ![WhatsApp web page][WhatsApp web page]{ .rounded }

    WhatsApp 是一款支持文字、语音和视频通讯的消息应用，可在多个平台上使用。它提供端到端加密以保护隐私，在全球广泛使用。
    
    点击 [:material-download-circle:][WhatsApp web site] 下载。

    [WeChat web site]: https://www.wechat.com/en/
    [WhatsApp web site]: https://www.whatsapp.com/
    [WeChat web page]: chat-groups/wechat-website-screenshot.png
    [WhatsApp web page]: chat-groups/whatsapp-website-screenshot.png

    </div>


您需要一个有效的手机号码来注册这两个平台的用户账号。通常，验证码会通过短信发送。注册过程需要保持网络连接。

??? warning "使用限制"

    微信和 WhatsApp 都有一些使用限制：

    由于美国制裁和数据政策，再加上中国的互联网限制，在中国大陆连接 WhatsApp 服务器比较困难。不过，可以通过使用合法的 VPN 解决这个问题。

    印度政府在2020年以国家安全为由禁止了微信。因此，在印度无法合法使用微信。

## 微信群

一个微信群最多可容纳500名成员。如果现有群达到人数上限，我们会开设新的群组。

扫描以下二维码加入我们的群组：

![WeChat group](chat-groups/wechat-group-2025.png){ load=lazy width="300" }

由于政策限制，当群成员超过200人时，微信会自动开启验证功能。
如果您无法直接加入群组，请添加以下管理员并请求邀请：

=== "ryan"

    ![][ryan]{ width="300" .rounded }


    [ryan]: chat-groups/admin-ryan.jpg

=== "echo"

    ![][echo]{ width="300" .rounded }


    [echo]: chat-groups/admin-echo.jpg


## WhatsApp 群组

每个 WhatsApp 群组最多可容纳256名成员。我们为每个硕士项目设立了五个频道的 WhatsApp 社群。

点击链接加入相应的硕士项目 WhatsApp 群组：

<div class="grid cards" markdown>

-   :material-satellite-uplink:{ .lg .middle } __通信工程__

    ---

    涵盖电信、射频工程和无线通信等重要主题。

    [:octicons-arrow-right-24: 一键加入][CME]

-   :material-robot:{ .lg .middle } __计算机控制与自动化__

    ---

    提供计算机控制和自动化系统的高级实用工具。

    [:octicons-arrow-right-24: 一键加入][CCA]

-   :simple-electron:{ .lg .middle } __电子工程__

    ---

    涵盖集成电路设计、微电子制造和电子光电产品制造。

    [:octicons-arrow-right-24: 一键加入][ET]

-   :material-wind-power:{ .lg .middle } __电力工程__

    ---

    面向在职工程师、研发经理、电力系统设计师或行业规划师。

    [:octicons-arrow-right-24: 一键加入][PE]

-   :material-sine-wave:{ .lg .middle } __信号处理与机器学习__

    ---

    面向对数字信号处理和人工智能感兴趣的工程师、设计师、数据科学家、研发经理和行业规划师。

    [:octicons-arrow-right-24: 一键加入][SPML]

    [CME]: https://chat.whatsapp.com/JtSWdnvkuFpDDUlMDfrN85
    [CCA]: https://chat.whatsapp.com/KTFePfashAx0jkFUvNJTRa
    [ET]: https://chat.whatsapp.com/DRjb461KM1eIvzWFAEvDxN
    [PE]: https://chat.whatsapp.com/FGtp2jLFPRFC7JoHVojCkO
    [SPML]: https://chat.whatsapp.com/LW7Bo2tKssGJ9qQC1HyOuj

</div>

!!! tip "温馨提示"

    每个频道的名额有限。请只加入 __您所在项目的频道__。