import {
  Observable,
  animationFrameScheduler,
  auditTime,
  fromEvent,
  map,
  merge,
  startWith
} from "rxjs"

/* ----------------------------------------------------------------------------
 * Types
 * ------------------------------------------------------------------------- */

/**
 * Element offset
 */
export interface ElementOffset {
  x: number                            /* Horizontal offset */
  y: number                            /* Vertical offset */
}

/* ----------------------------------------------------------------------------
 * Functions
 * ------------------------------------------------------------------------- */

/**
 * Retrieve element offset
 *
 * @param el - Element
 *
 * @returns Element offset
 */
export function getElementOffset(
  el: HTMLElement
): ElementOffset {
  return {
    x: el.offsetLeft,
    y: el.offsetTop
  }
}

/* ------------------------------------------------------------------------- */

/**
 * Watch element offset
 *
 * @param el - Element
 *
 * @returns Element offset observable
 */
export function watchElementOffset(
  el: HTMLElement
): Observable<ElementOffset> {
  return merge(
    fromEvent(window, "load"),
    fromEvent(window, "resize")
  )
    .pipe(
      auditTime(0, animationFrameScheduler),
      map(() => getElementOffset(el)),
      startWith(getElementOffset(el))
    )
}
