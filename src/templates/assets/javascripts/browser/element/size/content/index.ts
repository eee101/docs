import { ElementSize } from "../_"

/* ----------------------------------------------------------------------------
 * Functions
 * ------------------------------------------------------------------------- */

/**
 * Retrieve element content size (= scroll width and height)
 *
 * @param el - Element
 *
 * @returns Element content size
 */
export function getElementContentSize(
  el: HTMLElement
): ElementSize {
  return {
    width:  el.scrollWidth,
    height: el.scrollHeight
  }
}

/**
 * Retrieve the overflowing container of an element, if any
 *
 * @param el - Element
 *
 * @returns Overflowing container or nothing
 */
export function getElementContainer(
  el: HTMLElement
): HTMLElement | undefined {
  let parent = el.parentElement
  while (parent)
    if (
      el.scrollWidth  <= parent.scrollWidth &&
      el.scrollHeight <= parent.scrollHeight
    )
      parent = (el = parent).parentElement
    else
      break

  /* Return overflowing container */
  return parent ? el : undefined
}
