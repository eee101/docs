import {
  Observable,
  filter,
  finalize,
  map,
  mergeMap,
  skip,
  switchMap,
  take,
  takeUntil
} from "rxjs"

import {
  getElements,
  watchElementVisibility
} from "~/browser"
import { mountTooltip } from "~/components"

/* ----------------------------------------------------------------------------
 * Helper types
 * ------------------------------------------------------------------------- */

/**
 * Patch options
 */
interface PatchOptions {
  document$: Observable<Document>      /* Document observable */
}

/* ----------------------------------------------------------------------------
 * Functions
 * ------------------------------------------------------------------------- */

/**
 * Patch ellipsis
 *
 * This function will fetch all elements that are shortened with ellipsis, and
 * filter those which are visible. Once they become visible, they stay in that
 * state, even though they may be hidden again. This optimization is necessary
 * to reduce pressure on the browser, with elements fading in and out of view.
 *
 * @param options - Options
 */
export function patchEllipsis(
  { document$ }: PatchOptions
): void {
  document$
    .pipe(
      switchMap(() => getElements(".md-ellipsis")),
      mergeMap(el => watchElementVisibility(el)
        .pipe(
          takeUntil(document$.pipe(skip(1))),
          filter(visible => visible),
          map(() => el),
          take(1)
        )
      ),
      filter(el => el.offsetWidth < el.scrollWidth),
      mergeMap(el => {
        const text = el.innerText
        const host = el.closest("a") || el
        host.title = text

        /* Mount tooltip */
        return mountTooltip(host)
          .pipe(
            takeUntil(document$.pipe(skip(1))),
            finalize(() => host.removeAttribute("title"))
          )
      })
    )
      .subscribe()

  // @todo move this outside of here and fix memleaks
  document$
    .pipe(
      switchMap(() => getElements(".md-status")),
      mergeMap(el => mountTooltip(el))
    )
      .subscribe()
}
